# recipe-app-api-proxy

An NGINX proxy for a recipe app api

## Usage

### Environment variables
* `LISTEN_PORT` - Port to listen on (default:`8000`)
* `APP_HOST` - Hostname of APP to forward request to (default: `app`s)
* `APP_PORT` - Port of app to forward requests to (default:`9000`)